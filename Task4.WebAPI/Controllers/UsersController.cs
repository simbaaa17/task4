﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Task4.BLL.Services;
using Task4.Common.DTO.User;
using Task4.DAL.Entities;

namespace Task4.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly UserService _userService;

        public UsersController(UserService userService)
        {
            _userService = userService ?? throw new ArgumentNullException(nameof(userService));
        }

        [HttpGet]
        public ActionResult<ICollection<UserDTO>> Get()
        {
            return Ok(_userService.GetUsers());
        }

        [HttpGet("tasks")]
        public ActionResult<ICollection<UserWithTasksDTO>> GetUsersWithTasks()
        {
            return Ok(_userService.GetUsersWithTasks());
        }

        [HttpGet("{id}")]
        public ActionResult<UserDTO> GetById(int id)
        {
            return Ok(_userService.GetUserById(id));
        }

        [HttpGet("info/{id}")]
        public ActionResult<UserDTO> GetFullInformationById(int id)
        {
            return Ok( _userService.GetUserFullInformation(id));
        }


        [HttpPost]
        public ActionResult<UserDTO> CreateProject([FromBody] UserCreateDTO dto)
        {
            return Ok(_userService.CreateUser(dto));
        }


        [HttpPut]
        public ActionResult Put([FromBody] UserDTO user)
        {
            _userService.UpdateUser(user);
            return NoContent();
        }

        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            _userService.DeleteUser(id);
            return NoContent();
        }
    }
}
