﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Task4.BLL.Services;
using Task4.Common.DTO.Team;

namespace Task4.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TeamsController : ControllerBase
    {
        private readonly TeamService _teamService;

        public TeamsController(TeamService teamService)
        {
            _teamService = teamService ?? throw new ArgumentNullException(nameof(TeamService));
        }

        [HttpGet]
        public ActionResult<ICollection<TeamDTO>> Get()
        {
            return Ok(_teamService.GetTeams());
        }


        [HttpGet("info")]
        public ActionResult<ICollection<TeamDTO>> GetMembers()
        {
            return Ok(_teamService.GetMembersForTeams());
        }

        [HttpGet("{id}")]
        public ActionResult<TeamDTO> GetById(int id)
        {
            return Ok(_teamService.GetTeamById(id));
        }

        [HttpPost]
        public ActionResult<TeamDTO> CreateProject([FromBody] TeamCreateDTO dto)
        {
            return Ok(_teamService.CreateTeam(dto));
        }


        [HttpPut]
        public ActionResult Put([FromBody] TeamDTO team)
        {
            _teamService.UpdateTeam(team);
            return NoContent();
        }

        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            _teamService.DeleteTeam(id);
            return NoContent();
        }
    }
}
