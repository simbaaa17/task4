﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Task4.BLL.Services;
using Task4.Common.DTO.Project;

namespace Task4.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectsController : ControllerBase
    {
        private readonly ProjectService _projectService;

        public ProjectsController(ProjectService projectService)
        {
            _projectService = projectService ?? throw new ArgumentNullException(nameof(projectService));
        }

        [HttpGet]
        public ActionResult<ICollection<ProjectDTO>> Get()
        {
            return Ok(_projectService.GetProjects());
        }


        [HttpGet("info")]
        public ActionResult<ICollection<ProjectInformationFullDTO>> GetFullInformation()
        {
            return Ok( _projectService.GetProjectsFullInformation());
        }

        [HttpGet("{id}")]
        public ActionResult<ProjectDTO> GetById(int id)
        {
            return Ok(_projectService.GetProjectById(id));
        }

        [HttpPost]
        public ActionResult<ProjectDTO> CreateProject([FromBody] ProjectCreateDTO dto)
        {
            return Ok(_projectService.CreateProject(dto));
        }

        [HttpPut]
        public ActionResult Put([FromBody] ProjectDTO Project)
        {
            _projectService.UpdateProject(Project);
            return NoContent();
        }

        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            _projectService.DeleteProject(id);
            return NoContent();
        }
    }
}
