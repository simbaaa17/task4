﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Task4.BLL.Services;
using Task4.Common.DTO.Task;

namespace Task4.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TasksController : ControllerBase
    {
        private readonly TaskService _taskService;

        public TasksController(TaskService taskService)
        {
            _taskService = taskService ?? throw new ArgumentNullException(nameof(TaskService));
        }

        [HttpGet]
        public ActionResult<ICollection<TaskDTO>> Get()
        {
            return Ok(_taskService.GetTasks());
        }

        [HttpGet("byUser/{Id}")]
        public ActionResult<ICollection<TaskDTO>> GetTasksByUserId(int id)
        {
            return Ok(_taskService.GetTasksForUser(id));
        }

        [HttpGet("inProject/byUser/{Id}")]
        public ActionResult<TasksCountForUserDTO> GetTasksInProjectByUserId(int id)
        {
            return Ok(_taskService.GetTasksCountForUser(id));
        }

        [HttpGet("info/byUser/{Id}")]
        public ActionResult<ICollection<TaskInformationShortDTO>> GetTasksShortInfoByUserId(int id)
        {
            return Ok(_taskService.GetTasksFinishedInCurrentYear(id));
        }


        [HttpGet("{id}")]
        public ActionResult<TaskDTO> GetById(int id)
        {
            return Ok(_taskService.GetTaskById(id));
        }


        [HttpPost]
        public ActionResult<TaskDTO> CreateProject([FromBody] TaskCreateDTO dto)
        {
            return Ok(_taskService.CreateTask(dto));
        }

        [HttpPut]
        public ActionResult Put([FromBody] TaskDTO Task)
        {
            _taskService.UpdateTask(Task);
            return NoContent();
        }

        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            _taskService.DeleteTask(id);
            return NoContent();
        }
    }
}
