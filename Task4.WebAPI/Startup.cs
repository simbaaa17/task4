using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Task4.BLL.Services;
using Task4.DAL;
using Task4.DAL.Context;
using Task4.DAL.Entities;
using Task4.DAL.Repositories;
using Task4.DAL.Repositories.Abstract;

namespace Task4.WebAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers().
                AddNewtonsoftJson(options =>
                    options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore);

            services.AddDbContext<ApplicationDbContext>();

            var application = typeof(UserService).Assembly;

            services.AddAutoMapper(application);

            services.AddTransient<IEntityRepository<DAL.Entities.Task>, TaskRepository>();
            services.AddTransient<IEntityRepository<User>, UserRepository>();
            services.AddTransient<IEntityRepository<Team>, TeamRepository>();
            services.AddTransient<IEntityRepository<Project>, ProjectRepository>();

            services.AddTransient<IUnitOfWork, UnitOfWork>();

            services.AddTransient<UserService>();
            services.AddTransient<TeamService>();
            services.AddTransient<TaskService>();
            services.AddTransient<ProjectService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
