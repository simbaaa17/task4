﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Task4.DAL.Repositories.Abstract
{
    public interface IEntityRepository<T> where T : class
    {
        void SaveChanges();
        T FirstOrDefault(Expression<Func<T, bool>> predicate);
        T FirstOrDefault(Expression<Func<T, bool>> predicate, string[] includes);
        T FirstOrDefault();

        IQueryable<T> GetAll(string[] includes = null);
        IQueryable<T> FindBy(Expression<Func<T, bool>> predicate);
        bool Any(Expression<Func<T, bool>> predicate);
        T Find(params object[] keys);
        void Add(T entity);
        void AddRange(IEnumerable<T> entities);
        void Delete(T entity);
        void DeleteRange(IEnumerable<T> entity);
        void Update(T entity);
        IOrderedQueryable<T> OrderBy<K>(Expression<Func<T, K>> predicate);
        IQueryable<IGrouping<K, T>> GroupBy<K>(Expression<Func<T, K>> predicate);

    }
}
