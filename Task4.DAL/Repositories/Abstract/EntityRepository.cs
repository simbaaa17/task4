﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Task4.DAL.Context;

namespace Task4.DAL.Repositories.Abstract
{
    public class EntityRepository<TEntity> : IEntityRepository<TEntity> 
        where TEntity : class
    {
        protected readonly DbSet<TEntity> _dbSet;
        protected readonly ApplicationDbContext _context;

        public EntityRepository(ApplicationDbContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _dbSet = context.Set<TEntity>();
        } 
        public virtual TEntity First(Expression<Func<TEntity, bool>> predicate)
            => _dbSet.First(predicate);
 
        public virtual TEntity FirstOrDefault(Expression<Func<TEntity, bool>> predicate)
            => _dbSet.FirstOrDefault(predicate);
          

        public TEntity FirstOrDefault()
            => _dbSet.FirstOrDefault();
        

        public virtual IQueryable<TEntity> GetAll()
            => _dbSet.AsNoTracking();

        public IQueryable<TEntity> GetAll(string[] includes)
        {
            return includes.Aggregate(_dbSet.AsQueryable(), (query, path) => query.Include(path));
        }


        public virtual IQueryable<TEntity> FindBy(Expression<Func<TEntity, bool>> predicate)
            => _dbSet.Where(predicate);
        

        public bool Any(Expression<Func<TEntity, bool>> predicate)
            => _dbSet.Any(predicate);
        
        public virtual TEntity Find(params object[] keys)
            => _dbSet.Find(keys);
        

        public virtual void Add(TEntity entity)
            => _dbSet.Add(entity);
        
        public virtual void AddRange(IEnumerable<TEntity> entities)
            => _dbSet.AddRange(entities);
        

        public virtual void Delete(TEntity entity)
            => _dbSet.Remove(entity);

        public void DeleteRange(IEnumerable<TEntity> entity)
            => _dbSet.RemoveRange(entity);

        public virtual void Update(TEntity entity)
        {
            _context.Entry(entity).State = EntityState.Modified;
        }
        public virtual void SaveChanges()
        {
            _context.SaveChanges();
        }
        public Task<TEntity> FirstOrDefaultAsync(Expression<Func<TEntity, bool>> predicate, string[] includes)
        {
            return includes.Aggregate(_dbSet.AsQueryable(), (query, path) 
                => query.Include(path))
                .FirstOrDefaultAsync(predicate);
        }

        public TEntity FirstOrDefault(Expression<Func<TEntity, bool>> predicate, string[] includes)
        {
            return includes.Aggregate(_dbSet.AsQueryable(), (query, path)
                => query.Include(path))
                .FirstOrDefault(predicate);
        }

        public IOrderedQueryable<TEntity> OrderBy<K>(Expression<Func<TEntity, K>> predicate)
            => _dbSet.OrderBy(predicate);

        public IQueryable<IGrouping<K, TEntity>> GroupBy<K>(Expression<Func<TEntity, K>> predicate)
            => _dbSet.GroupBy(predicate);

    }
}
