﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Task4.Common.DTO.Project;
using Task4.Common.DTO.Task;
using Task4.DAL.Context;
using Task4.DAL.Entities;
using Task4.DAL.Repositories.Abstract;

namespace Task4.DAL.Repositories
{
    
    public class TaskRepository : EntityRepository<Task>
    {
        private readonly IMapper _mapper;

        public TaskRepository(ApplicationDbContext context, IMapper mapper) : base(context)
        {
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public TasksCountForUserDTO GetTasksCountForUser(int userId)
        {
            return new TasksCountForUserDTO
            {
                Tasks = _context.Projects
               .ToDictionary(k => GetProjectDTOById(k.Id), i => i.Tasks.Where(t => t.Performer.Id == userId)
               .Count())
            };
        }
        public ICollection<TaskDTO> GetTasksForUser(int userId)
        {
            return _context.Projects
                    .SelectMany(r => r.Tasks)
                    .Where(t => t.Performer.Id == userId && t.Name.Length < 45)
                    .Select(t => GetTaskDTO(t.Id)).ToList();
        }

        public ICollection<TaskInformationShortDTO> GetTasksFinishedInCurrentYear(int userId)
                => _context.Projects
                    .SelectMany(r => r.Tasks)
                    .Where(t => t.Performer.Id == userId && t.FinishedAt.Year == 2020)
                    .Select(t => new TaskInformationShortDTO
                    {
                        Id = t.Id,
                        Name = t.Name
                    }).ToList();

        private ProjectDTO GetProjectDTOById(int id)
          => _mapper.Map<ProjectDTO>(_context.Projects
              .FirstOrDefault(p => p.Id == id));

        private TaskDTO GetTaskDTO(int id)
           => _mapper.Map<TaskDTO>(_context.Tasks
               .FirstOrDefault(t => t.Id == id));

    }
}
