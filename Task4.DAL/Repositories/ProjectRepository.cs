﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Task4.Common.DTO.Project;
using Task4.Common.DTO.Task;
using Task4.DAL.Context;
using Task4.DAL.Entities;
using Task4.DAL.Repositories.Abstract;

namespace Task4.DAL.Repositories
{
    public class ProjectRepository : EntityRepository<Project>
    {
        private readonly IMapper _mapper;
        public ProjectRepository(ApplicationDbContext context, IMapper mapper) : base(context)
        {
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }
        public ICollection<ProjectInformationFullDTO> GetProjectsFullInformation()
                   => (from project in _context.Projects.ToList()
                       let tasksInProject = from task in _context.Tasks.ToList()
                                            where task.ProjectId == project.Id
                                            select task
                       select new ProjectInformationFullDTO
                       {
                           Project = GetProjectDTOById(project.Id),
                           LongestTask = _mapper.Map<TaskDTO>((from task in tasksInProject
                                                               orderby task.Description.Length descending
                                                               select task).FirstOrDefault()),
                           ShortestTask = _mapper.Map<TaskDTO>((from task in tasksInProject
                                                                orderby task.Name.Length ascending
                                                                select task).FirstOrDefault()),
                           TeamCount = (project.Description.Length > 20 || _context.Tasks.Count() < 3)
                                           ? (from task in project.Tasks.ToList()
                                              select task.Performer).Count()
                                           : 0
                       }).ToList();

        private ProjectDTO GetProjectDTOById(int id)
          => _mapper.Map<ProjectDTO>(_context.Projects
              .FirstOrDefault(p => p.Id == id));

    }
}
