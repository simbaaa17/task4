﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Task4.DAL.Context;
using Task4.DAL.Entities;
using Task4.DAL.Repositories.Abstract;
using AutoMapper;
using Task4.Common.DTO.Task;
using System.Threading.Tasks;
using Task4.Common.DTO.Project;
using Microsoft.EntityFrameworkCore;
using Task4.Common.DTO.User;

namespace Task4.DAL.Repositories
{
    public class UserRepository : EntityRepository<User>
    {
        private readonly IMapper _mapper;
        public UserRepository(ApplicationDbContext context, IMapper mapper) : base(context)
        {
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }


        public ICollection<UserWithTasksDTO> GetUsersWithTasks()
        {
            return (from performer in _context.Users.ToList()
                    join task in _context.Tasks.ToList() on performer.Id equals task.PerformerId
                    orderby performer.Firstname
                    group task by performer into g
                    select new UserWithTasksDTO
                    {
                        User = _mapper.Map<UserDTO>(g.Key),
                        Tasks = _mapper.Map<ICollection<TaskDTO>>(from task in g
                                                                  orderby task.Name.Length descending
                                                                  select task).ToList()
                    }).ToList();
        }

        public UserInformationFullDTO GetUserInformationFullById(int id)
        {
            var inProcess = new List<TaskState>() { TaskState.Canceled, TaskState.Started, TaskState.Created };

            return (from user in _context.Users.ToList()
                    where user.Id == id
                    let allProjectsForUser = from project in _context.Projects.ToList()
                                             where project.AuthorId == id
                                             orderby project.CreatedAt descending
                                             select project
                    let allTasksForUser = from task in _context.Tasks.ToList()
                                          where task.PerformerId == id
                                          select task
                    select new UserInformationFullDTO
                    {
                        User = _mapper.Map<UserDTO>(user),
                        LastProject = _mapper.Map<ProjectDTO>(allProjectsForUser.FirstOrDefault()),
                        TasksCount = (from task in allTasksForUser
                                      where task.ProjectId == _context.Projects.ToList().FirstOrDefault().Id
                                      select task).Count(),
                        TasksCanceledCount = (from task in allTasksForUser
                                              where inProcess.Contains(task.TaskState)
                                              select task).Count(),
                        LongestTask = _mapper.Map<TaskDTO>((from task in allTasksForUser
                                                            orderby (task.FinishedAt - task.CreatedAt) descending
                                                            select task).FirstOrDefault())
                    }).FirstOrDefault();
        }
    }
}
