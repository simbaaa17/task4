﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Task4.Common.DTO.Team;
using Task4.DAL.Context;
using Task4.DAL.Entities;
using Task4.DAL.Repositories.Abstract;
using Task4.Common.DTO.User;
using AutoMapper;

namespace Task4.DAL.Repositories
{
    public class TeamRepository : EntityRepository<Team>
    {
        private readonly IMapper _mapper;
        public TeamRepository(ApplicationDbContext context, IMapper mapper) : base(context)
        {
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public ICollection<TeamInformationFullDTO> GetMembersForTeams()
               => (from team in _context.Teams.ToList()
                   join member in _context.Users.ToList() on team.Id equals member.TeamId
                   where (DateTime.Now.Year - member.Birthday.Year) > 10
                   orderby member.RegisteredAt descending
                   group member by new { team.Id, team.Name } into g
                   select new TeamInformationFullDTO
                   {
                       Id = g.Key.Id,
                       Name = g.Key.Name,
                       Members = _mapper.Map<ICollection<UserDTO>>(from member in g select member)
                   }).ToList();

    }
}
