﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task4.DAL.Entities
{
    public enum TaskState
    {
        Created = 0,
        Started,
        Finished,
        Canceled
    }
}
