﻿using System;
using System.Collections.Generic;
using System.Text;
using Task4.DAL.Entities.Abstract;

namespace Task4.DAL.Entities
{
    public class Task : BaseEntity
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public DateTime FinishedAt { get; set; }

        public TaskState TaskState { get; set; }

        public int ProjectId { get; set; }
        public Project Project { get; set; } 

        public int PerformerId { get; set; }
        public User Performer { get; set; }

        public void ChangeName(string name)
        {
            if (string.IsNullOrEmpty(name))
                throw new ArgumentException("message", nameof(name));

            Name = name;
        }

        public void ChangeDescription(string description)
        {
            if (string.IsNullOrEmpty(description))
                throw new ArgumentException("message", nameof(description));

            Description = description;
        }

        public void ChangeFinishedDate(DateTime finishedAt) => FinishedAt = finishedAt;

        public void ChangeTaskState(TaskState state) => TaskState = state;

        public void ChangeProject(int projectId) => ProjectId = projectId;

        public void ChangePerformer(int performerId) => PerformerId = performerId;
    }
}
