﻿using System;
using System.Collections.Generic;
using System.Text;
using Task4.DAL.Entities.Abstract;

namespace Task4.DAL.Entities
{
    public class Team : BaseEntity
    {
        public string Name { get; set; }

        public ICollection<User> Members { get; private set; }

        public ICollection<Project> Projects { get; private set; }

        public Team()
        {
            Projects = new List<Project>();
            Members = new List<User>();
        }

        public void ChangeName(string name)
        {
            if (string.IsNullOrEmpty(name))
                throw new ArgumentException("message", nameof(name));

            Name = name;
        }
    }
}
