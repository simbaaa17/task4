﻿using System;
using System.Collections.Generic;
using System.Text;
using Task4.DAL.Entities.Abstract;

namespace Task4.DAL.Entities
{
    public class TaskStateModel : BaseEntity
    {
        public string Value { get; set; }
    }
}
