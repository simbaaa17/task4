﻿using System;
using System.Collections.Generic;
using System.Text;
using Task4.DAL.Entities.Abstract;

namespace Task4.DAL.Entities
{
    public class Project : BaseEntity
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public DateTime Deadline { get; set; }

        public int AuthorId { get; set; }
        public User Author { get; set; } 

        public int TeamId { get; set; }
        public Team Team { get; set; } 

        public ICollection<Task> Tasks { get; private set; }

        public Project()
        {
            Tasks = new List<Task>();
        }

        public void ChangeName(string name)
        {
            if (string.IsNullOrEmpty(name))
                throw new ArgumentException("message", nameof(name));

            Name = name;
        }

        public void ChangeDescription(string description)
        {
            if (string.IsNullOrEmpty(description))
                throw new ArgumentException("message", nameof(description));

            Description = description;
        }

        public void ChangeDeadline(DateTime deadline) => Deadline = deadline;

        public void ChangeAuthor(int authorId) => AuthorId = authorId;

        public void ChangeTeam(int teamId) => TeamId = teamId;

    }
}
