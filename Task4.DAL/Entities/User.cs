﻿using System;
using System.Collections.Generic;
using System.Text;
using Task4.DAL.Entities.Abstract;

namespace Task4.DAL.Entities
{
    public class User : BaseEntity
    {
        public string Firstname { get; set; }

        public string Lastname { get; set; }

        public string Email { get; set; }

        public DateTime Birthday { get; set; }

        public DateTime RegisteredAt { get; set; }

        public int? TeamId { get; set; }
        public Team Team { get; set; }

        public ICollection<Task> Tasks { get; private set; }

        public ICollection<Project> Projects { get; private set; }

        public User()
        {
            Tasks = new List<Task>();
            Projects = new List<Project>();
        }

        public void ChangeFirstname(string firstname)
        {
            if (string.IsNullOrEmpty(firstname))
                throw new ArgumentException("message", nameof(firstname));

            Firstname = firstname;
        }

        public void ChangeLastname(string lastname)
        {
            if (string.IsNullOrEmpty(lastname))
                throw new ArgumentException("message", nameof(lastname));

            Lastname = lastname;
        }

        public void ChangeEmail(string email)
        {
            if (string.IsNullOrEmpty(email))
                throw new ArgumentException("message", nameof(email));

            Email = email;
        }

        public void ChangeBirthdayDate(DateTime birthdayDate) => Birthday = birthdayDate;

        public void ChangeRegisteredDate(DateTime registeredAt) => RegisteredAt = registeredAt;

        public void ChangeTeam(int teamId) => TeamId = teamId;
    }
}
