﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using Task4.DAL.Entities;

namespace Task4.DAL.Configurations
{
    public class TeamConfiguration : IEntityTypeConfiguration<Team>
    {
        public void Configure(EntityTypeBuilder<Team> builder)
        {
            builder.ToTable("Teams").HasKey(team => team.Id);
            builder.Property(team => team.Name).IsRequired().HasMaxLength(30);
        }
    }
}
