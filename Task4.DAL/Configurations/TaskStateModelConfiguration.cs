﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using Task4.DAL.Entities;

namespace Task4.DAL.Configurations
{
    public class TaskStateModelConfiguration : IEntityTypeConfiguration<TaskStateModel>
    {
        public void Configure(EntityTypeBuilder<TaskStateModel> builder)
        {
            builder.ToTable("TaskStates").HasKey(taskState => taskState.Id);
            builder.Property(taskState => taskState.Value).IsRequired().HasMaxLength(30);
        }
    }
}
