﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using Task4.DAL.Entities;

namespace Task4.DAL.Configurations
{
    public class UserConfiguration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.ToTable("Users").HasKey(user => user.Id);
            builder.Property(user => user.Firstname).IsRequired().HasMaxLength(30);
            builder.Property(user => user.Lastname).IsRequired().HasMaxLength(30);
            builder.Property(user => user.Email).IsRequired().HasMaxLength(30);
        }
    }
}
