﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using Task4.DAL.Entities;

namespace Task4.DAL.Configurations
{
    public class TaskConfiguration : IEntityTypeConfiguration<Task>
    {
        public void Configure(EntityTypeBuilder<Task> builder)
        {
            builder.ToTable("Tasks").HasKey(task => task.Id);
            builder.Property(task => task.Name).IsRequired().HasMaxLength(30);
            builder.Property(task => task.Description).IsRequired().HasMaxLength(100);
        }
    }
}
