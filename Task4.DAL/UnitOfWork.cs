﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Task4.DAL.Context;
using Task4.DAL.Repositories.Abstract;

namespace Task4.DAL
{
    public class UnitOfWork : IUnitOfWork 
    {
        private bool _disposed;
        private Dictionary<Type, object> _repositories;
        private readonly IServiceProvider _services;

        public ApplicationDbContext DbContext { get; }

        public UnitOfWork(ApplicationDbContext dbContext, IServiceProvider services)
        {
            DbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
            _services = services ?? throw new ArgumentNullException(nameof(services));
        }

        public void Dispose()
        {
            Dispose(true);

            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    // clear repositories
                    _repositories?.Clear();

                    // dispose the db context.
                    DbContext.Dispose();
                }
            }

            _disposed = true;
        }
        public IEntityRepository<TEntity> GetRepository<TEntity>(bool hasCustomRepository = false) where TEntity : class
        {
            if (_repositories is null)
                _repositories = new Dictionary<Type, object>();

            if (hasCustomRepository)
            {
                var customRepo = _services.GetService<IEntityRepository<TEntity>>();

                if (customRepo != null)
                    return customRepo;
            }

            var type = typeof(TEntity);

            if (!_repositories.ContainsKey(type))
                _repositories[type] = new EntityRepository<TEntity>(DbContext);
            

            return (IEntityRepository<TEntity>)_repositories[type];
        }

        public int Commit() => DbContext.SaveChanges();
        
    }
}
