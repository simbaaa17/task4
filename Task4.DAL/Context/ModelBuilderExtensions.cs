﻿using Bogus;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using Task4.DAL.Entities;

namespace Task4.DAL.Context
{
    public static class ModelBuilderExtensions
    {
        private const int ENTITY_COUNT = 50;

        public static void Configure(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Project>()
                .HasOne(pr => pr.Author)
                .WithMany(u => u.Projects)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Project>()
                .HasOne(pr => pr.Team)
                .WithMany(t => t.Projects)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Task>()
                .HasOne(task => task.Performer)
                .WithMany(u => u.Tasks)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Task>()
                .HasOne(task => task.Project)
                .WithMany(p => p.Tasks)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<User>()
                .HasOne(user => user.Team)
                .WithMany(t => t.Members)
                .OnDelete(DeleteBehavior.Restrict);
        }

        public static void Seed(this ModelBuilder modelBuilder)
        {
            var teams = GenerateRandomTeams();
            var users = GenerateRandomUsers(teams);
            var projects = GenerateRandomProjects(users, teams);
            var tasks = GenerateRandomTasks(users, projects);

            modelBuilder.Entity<Team>().HasData(teams);
            modelBuilder.Entity<User>().HasData(users);
            modelBuilder.Entity<Project>().HasData(projects);
            modelBuilder.Entity<Task>().HasData(tasks);
        }

    
        public static ICollection<Team> GenerateRandomTeams()
        {
            int teamId = 1;

            var teamsFake = new Faker<Team>()
                .RuleFor(pi => pi.Id, f => teamId++)
                .RuleFor(pi => pi.Name, f => f.Person.FullName)
                .RuleFor(pi => pi.CreatedAt, f => DateTime.Now)
                .RuleFor(pi => pi.UpdatedAt, f => DateTime.Now);

            return teamsFake.Generate(ENTITY_COUNT);
        }

        public static ICollection<User> GenerateRandomUsers(ICollection<Team> teams)
        {
            int userId = 1;

            var usersFake = new Faker<User>()
                .RuleFor(pi => pi.Id, f => userId++)
                .RuleFor(pi => pi.Firstname, f => f.Person.FirstName)
                .RuleFor(pi => pi.Lastname, f => f.Person.LastName)
                .RuleFor(pi => pi.Email, f => f.Person.Email)
                .RuleFor(pi => pi.Birthday, f => f.Person.DateOfBirth)
                .RuleFor(pi => pi.RegisteredAt, f => DateTime.Now)
                .RuleFor(pi => pi.TeamId, f => f.PickRandom(teams).Id )
                .RuleFor(pi => pi.CreatedAt, f => DateTime.Now)
                .RuleFor(pi => pi.UpdatedAt, f => DateTime.Now);

            return usersFake.Generate(ENTITY_COUNT);
        }

        public static ICollection<Project> GenerateRandomProjects(ICollection<User> users, ICollection<Team> teams)
        {
            int projectId = 1;

            var projectsFake = new Faker<Project>()
                .RuleFor(pi => pi.Id, f => projectId++)
                .RuleFor(pi => pi.Name, f => f.Person.FullName)
                .RuleFor(pi => pi.Description, f => f.Person.FullName)
                .RuleFor(pi => pi.Deadline, f => f.Date.Soon())
                .RuleFor(pi => pi.AuthorId, f => f.PickRandom(users).Id)
                .RuleFor(pi => pi.TeamId, f => f.PickRandom(teams).Id)
                .RuleFor(pi => pi.CreatedAt, f => DateTime.Now)
                .RuleFor(pi => pi.UpdatedAt, f => DateTime.Now);

            return projectsFake.Generate(ENTITY_COUNT);
        }

        public static ICollection<Task> GenerateRandomTasks(ICollection<User> users, ICollection<Project> projects)
        {
            int taskId = 1;

            var tasksFake = new Faker<Task>()
                .RuleFor(pi => pi.Id, f => taskId++)
                .RuleFor(pi => pi.Name, f => f.Person.FullName)
                .RuleFor(pi => pi.Description, f => f.Person.FullName)
                .RuleFor(pi => pi.FinishedAt, f => f.Date.Soon())
                .RuleFor(pi => pi.TaskState, f => f.PickRandom<TaskState>())
                .RuleFor(pi => pi.ProjectId, f => f.PickRandom(projects).Id)
                .RuleFor(pi => pi.PerformerId, f => f.PickRandom(users).Id)
                .RuleFor(pi => pi.CreatedAt, f => DateTime.Now)
                .RuleFor(pi => pi.UpdatedAt, f => DateTime.Now);

            return tasksFake.Generate(ENTITY_COUNT);
        }






    }
}
