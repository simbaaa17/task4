﻿using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using Task4.DAL.Configurations;
using Task4.DAL.Entities;

namespace Task4.DAL.Context
{
    public class ApplicationDbContext : DbContext
    {
        public DbSet<Project> Projects { get; private set; }
        public DbSet<Task> Tasks { get; private set; }
        public DbSet<TaskStateModel> TaskStateModels { get; private set; }
        public DbSet<Team> Teams { get; private set; }
        public DbSet<User> Users { get; private set; }
        public ApplicationDbContext()
        {
            Database.EnsureCreated();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Server=(localdb)\\mssqllocaldb;Database=task4db;Trusted_Connection=True;");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //modelBuilder.ApplyConfiguration(new ProjectConfiguration());
            //modelBuilder.ApplyConfiguration(new TaskConfiguration());
            //modelBuilder.ApplyConfiguration(new TaskStateModelConfiguration());
            //modelBuilder.ApplyConfiguration(new TeamConfiguration());
            //modelBuilder.ApplyConfiguration(new UserConfiguration());

            modelBuilder.Configure();

            modelBuilder.Seed();
            

            base.OnModelCreating(modelBuilder);
           
        }
    }
}
