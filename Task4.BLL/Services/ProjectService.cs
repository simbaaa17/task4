﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using Task4.BLL.Exceptions;
using Task4.BLL.Services.Abstract;
using Task4.Common.DTO.Project;
using Task4.DAL;
using Task4.DAL.Entities;
using Task4.DAL.Repositories;

namespace Task4.BLL.Services
{
    public class ProjectService : BaseService
    {
        private readonly ProjectRepository _projectRepository;
        public ProjectService(IUnitOfWork unitOfWork, IMapper mapper) : base(unitOfWork, mapper)
        {
            _projectRepository = (ProjectRepository)unitOfWork.GetRepository<Project>(true);
        }

        public ICollection<ProjectDTO> GetProjects()
        {
            var projects =  _unitOfWork
               .GetRepository<Project>()
               .GetAll(new string[] { "Author", "Team", "Tasks" })
               .ToList();

            return _mapper.Map<ICollection<ProjectDTO>>(projects);
        }

        public ICollection<ProjectInformationFullDTO> GetProjectsFullInformation() => _projectRepository.GetProjectsFullInformation();

        public ProjectDTO GetProjectById(int id)
        {
            var project = _unitOfWork.GetRepository<Project>()
                .FirstOrDefault(project => project.Id == id, new string[] { "Author", "Team", "Tasks" });

            if (project is null)
                throw new NotFoundException(nameof(project), id);

            return _mapper.Map<ProjectDTO>(project);
        }


        public ProjectDTO CreateProject(ProjectCreateDTO projectDto)
        {
            var projectEntity = _mapper.Map<Project>(projectDto);

            _projectRepository.Add(projectEntity);
            _unitOfWork.Commit();

            return _mapper.Map<ProjectDTO>(projectEntity);
        }


        public void UpdateProject(ProjectDTO projectDto)
        {
            var projectEntity = _unitOfWork.GetRepository<Project>()
                .FirstOrDefault(project => project.Id == projectDto.Id, new string[] { "Author", "Team", "Tasks" });

            if (projectEntity is null)
                throw new NotFoundException(nameof(Project), projectDto.Id);

            var timeNow = DateTime.Now;

            projectEntity.ChangeName(projectDto.Name);
            projectEntity.ChangeDescription(projectDto.Description);
            projectEntity.ChangeDeadline(projectDto.Deadline);
            projectEntity.ChangeAuthor(projectDto.Author.Id);

            projectEntity.UpdatedAt = timeNow;
            

            _projectRepository.Update(projectEntity);
            _unitOfWork.Commit();
        }

        public void DeleteProject(int projectId)
        {
            var projectEntity = _unitOfWork.GetRepository<Project>()
                .FirstOrDefault(project => project.Id == projectId);

            if (projectEntity is null)
                throw new NotFoundException(nameof(Project), projectId);

            _projectRepository.Delete(projectEntity);
            _unitOfWork.Commit();
        }
    }
}
