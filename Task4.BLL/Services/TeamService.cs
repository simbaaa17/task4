﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task4.BLL.Exceptions;
using Task4.BLL.Services.Abstract;
using Task4.Common.DTO.Team;
using Task4.Common.DTO.User;
using Task4.DAL;
using Task4.DAL.Context;
using Task4.DAL.Entities;
using Task4.DAL.Repositories;

namespace Task4.BLL.Services
{
    public class TeamService : BaseService
    {
        private readonly TeamRepository _teamRepository;
        public TeamService(IUnitOfWork unitOfWork, IMapper mapper) : base(unitOfWork, mapper)
        {
            _teamRepository = (TeamRepository)unitOfWork.GetRepository<Team>(true);
        }

        public ICollection<TeamDTO> GetTeams()
        {
            var teams = _unitOfWork.GetRepository<Team>()
                .GetAll(new string[] { "Projects", "Members" })
                .ToList();

            return _mapper.Map<ICollection<TeamDTO>>(teams);
        }

        public ICollection<TeamInformationFullDTO> GetMembersForTeams() => _teamRepository.GetMembersForTeams();


        public TeamDTO GetTeamById(int id)
        {
            var team = _unitOfWork.GetRepository<Team>()
                .FirstOrDefault(team => team.Id == id, new string[] { "Projects", "Members" });

            if (team is null)
                throw new NotFoundException(nameof(team), id);

            return _mapper.Map<TeamDTO>(team);
        }


        public TeamDTO CreateTeam(TeamCreateDTO TeamDto)
        {
            var teamEntity = _mapper.Map<Team>(TeamDto);

            _teamRepository.Add(teamEntity);
            _unitOfWork.Commit();

            return _mapper.Map<TeamDTO>(teamEntity);
        }

        public void UpdateTeam(TeamDTO teamDto)
        {
            var teamEntity = _unitOfWork.GetRepository<Team>()
                .FirstOrDefault(team => team.Id == teamDto.Id);

            if (teamEntity is null)
                throw new NotFoundException(nameof(Team), teamDto.Id);

            var timeNow = DateTime.Now;

            teamEntity.ChangeName(teamDto.Name);
        
            teamEntity.UpdatedAt = timeNow;

            _teamRepository.Update(teamEntity);
            _unitOfWork.Commit();
        }

        public void DeleteTeam(int teamId)
        {
            var teamEntity = _unitOfWork.GetRepository<Team>()
                .FirstOrDefault(team => team.Id == teamId);

            if (teamEntity is null)
                throw new NotFoundException(nameof(Team), teamId);

            _teamRepository.Delete(teamEntity);
            _unitOfWork.Commit();
        }
    }
}
