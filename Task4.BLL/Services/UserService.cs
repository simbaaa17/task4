﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task4.BLL.Exceptions;
using Task4.BLL.Services.Abstract;
using Task4.Common.DTO.Project;
using Task4.Common.DTO.Task;
using Task4.Common.DTO.User;
using Task4.DAL;
using Task4.DAL.Context;
using Task4.DAL.Entities;
using Task4.DAL.Repositories;

namespace Task4.BLL.Services
{
    public class UserService : BaseService
    {
        private readonly UserRepository _userRepository;
        public UserService(IUnitOfWork unitOfWork, IMapper mapper) : base(unitOfWork, mapper)
        {
            _userRepository = (UserRepository)unitOfWork.GetRepository<User>(true);
        }

        public ICollection<UserDTO> GetUsers()
        {
            var users = _unitOfWork.GetRepository<User>()
                .GetAll(new string[] { "Team", "Tasks"})
                .ToList();

            return _mapper.Map<ICollection<UserDTO>>(users);
        }

        public ICollection<UserWithTasksDTO> GetUsersWithTasks()
        {
            return _userRepository.GetUsersWithTasks();
        }

        public UserInformationFullDTO GetUserFullInformation(int id)
        {
            return _userRepository.GetUserInformationFullById(id);
        }


        public UserDTO GetUserById(int id)
        {
            var user = _unitOfWork.GetRepository<User>()
                .FirstOrDefault(user => user.Id == id, new string[] { "Team", "Tasks" });

            if (user is null)
                throw new NotFoundException(nameof(User), id);

            return _mapper.Map<UserDTO>(user);
        }


        public UserDTO CreateUser(UserCreateDTO userDto)
        {
            var userEntity = _mapper.Map<User>(userDto);

            _unitOfWork.GetRepository<User>().Add(userEntity);

            _unitOfWork.Commit();

            return _mapper.Map<UserDTO>(userEntity);
        }

     
        public void UpdateUser(UserDTO userDto)
        {
            var userEntity = _unitOfWork.GetRepository<User>()
               .FirstOrDefault(user => user.Id == userDto.Id, new string[] { "Team"} );
            
            if (userEntity is null)
                throw new NotFoundException(nameof(User), userDto.Id);

            var timeNow = DateTime.Now;

            userEntity.ChangeFirstname(userDto.Firstname);
            userEntity.ChangeLastname(userDto.Lastname);
            userEntity.ChangeBirthdayDate(userDto.Birthday);
            userEntity.ChangeEmail(userDto.Email);
            userEntity.ChangeTeam(userDto.Team.Id);

            userEntity.UpdatedAt = timeNow;


            _unitOfWork.GetRepository<User>().Update(userEntity);
            _unitOfWork.Commit();
        }

        public void DeleteUser(int userId)
        {
            var userEntity = _unitOfWork.GetRepository<User>()
               .FirstOrDefault(user => user.Id == userId);

            if (userEntity is null)
                throw new NotFoundException(nameof(User), userId);

            _unitOfWork.GetRepository<User>().Delete(userEntity);

            _unitOfWork.Commit();
        }
    }
}
