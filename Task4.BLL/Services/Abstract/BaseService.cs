﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using Task4.DAL;
using Task4.DAL.Context;

namespace Task4.BLL.Services.Abstract
{
    public abstract class BaseService
    {
        protected readonly IUnitOfWork _unitOfWork;
        protected readonly IMapper _mapper;

        public BaseService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork ?? throw new ArgumentNullException(nameof(unitOfWork));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }
    }
}
