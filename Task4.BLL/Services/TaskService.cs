﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task4.BLL.Exceptions;
using Task4.BLL.Services.Abstract;
using Task4.Common.DTO.Task;
using Task4.DAL;
using Task4.DAL.Context;
using Task4.DAL.Repositories;

namespace Task4.BLL.Services
{
    public class TaskService : BaseService
    {
        private readonly TaskRepository _taskRepository;
        public TaskService(IUnitOfWork unitOfWork, IMapper mapper) : base(unitOfWork, mapper)
        {
            _taskRepository = (TaskRepository)unitOfWork.GetRepository<DAL.Entities.Task>(true);
        }

        public ICollection<TaskDTO> GetTasks()
        {
            var tasks = _unitOfWork
                .GetRepository<DAL.Entities.Task>()
                .GetAll(new string[] { "Project", "Performer" })
                .ToList();

            return _mapper.Map<ICollection<TaskDTO>>(tasks);
        }

        public TasksCountForUserDTO GetTasksCountForUser(int userId) 
            => _taskRepository.GetTasksCountForUser(userId);
        public ICollection<TaskDTO> GetTasksForUser(int userId) 
            => _taskRepository.GetTasksForUser(userId);

        public ICollection<TaskInformationShortDTO> GetTasksFinishedInCurrentYear(int userId)
            => _taskRepository.GetTasksFinishedInCurrentYear(userId);


        public TaskDTO GetTaskById(int id)
        {
            var task = _unitOfWork.GetRepository<DAL.Entities.Task>()
                .FirstOrDefault(task => task.Id == id, new string[] { "Project", "Performer" });

            if (task is null)
                throw new NotFoundException(nameof(task), id);

            return _mapper.Map<TaskDTO>(task);
        }


        public TaskDTO CreateTask(TaskCreateDTO taskDto)
        {
            var taskEntity = _mapper.Map<DAL.Entities.Task>(taskDto);

            _taskRepository.Add(taskEntity);
            _unitOfWork.Commit();

            return _mapper.Map<TaskDTO>(taskEntity);
        }
      

        public void UpdateTask(TaskDTO taskDto)
        {
            var taskEntity = _unitOfWork.GetRepository<DAL.Entities.Task>()
                .FirstOrDefault(task => task.Id == taskDto.Id, new string[] { "Project", "Performer" });

            if (taskEntity is null)
                throw new NotFoundException(nameof(Task), taskDto.Id);

            var timeNow = DateTime.Now;

            taskEntity.ChangeName(taskDto.Name);
            taskEntity.ChangeDescription(taskDto.Description);
            taskEntity.ChangeFinishedDate(taskDto.FinishedAt);
            taskEntity.ChangePerformer(taskDto.Performer.Id);
            taskEntity.ChangeProject(taskDto.Project.Id);

            taskEntity.UpdatedAt = timeNow;

            _taskRepository.Update(taskEntity);
            _unitOfWork.Commit();
        }

        public void DeleteTask(int taskId)
        {
            var taskEntity = _unitOfWork.GetRepository<DAL.Entities.Task>()
               .FirstOrDefault(task => task.Id == taskId);

            if (taskEntity is null)
                throw new NotFoundException(nameof(Task), taskId);

            _taskRepository.Delete(taskEntity);
            _unitOfWork.Commit();
        }
    }
}
