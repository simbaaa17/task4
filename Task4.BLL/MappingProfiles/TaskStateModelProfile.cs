﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using Task4.Common.DTO.TaskState;
using Task4.DAL.Entities;

namespace Task4.BLL.MappingProfiles
{
    public class TaskStateModelProfile : Profile
    {
        public TaskStateModelProfile()
        {
            CreateMap<TaskStateModel, TaskStateModelDTO>();
        }
    }
}
