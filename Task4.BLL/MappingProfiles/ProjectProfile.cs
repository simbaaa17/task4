﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using Task4.Common.DTO.Project;
using Task4.DAL.Entities;

namespace Task4.BLL.MappingProfiles
{
    public sealed class ProjectProfile : Profile
    {
        public ProjectProfile()
        {
            CreateMap<Project, ProjectDTO>();
            //CreateMap<Project, ProjectDTO>()
            //    .ForMember(dest => dest.AuthorName, src => src.MapFrom(s => s.Author != null ? $"{s.Author.Lastname} {s.Author.Firstname}" : string.Empty))
            //    .ForMember(dest => dest.TeamName, src => src.MapFrom(s => s.Team != null ? s.Team.Name : string.Empty));

            CreateMap<ProjectDTO, Project>();
            CreateMap<ProjectCreateDTO, Project>();
          
        }
    }
}
