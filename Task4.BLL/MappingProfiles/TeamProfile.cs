﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using Task4.Common.DTO.Team;
using Task4.DAL.Entities;

namespace Task4.BLL.MappingProfiles
{
    public sealed class TeamProfile : Profile
    {
        public TeamProfile()
        {
            CreateMap<Team, TeamDTO>();
            CreateMap<TeamDTO, Team>();
            CreateMap<TeamCreateDTO, Team>();
        }
    }
}
