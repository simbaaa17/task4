﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using Task4.Common.DTO.User;
using Task4.DAL.Entities;

namespace Task4.BLL.MappingProfiles
{
    public sealed class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<User, UserDTO>();
            CreateMap<UserDTO, User>();
            CreateMap<UserCreateDTO, User>();
        }
    }
}
