﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using Task4.Common.DTO.Task;
using Task4.Common.DTO.TaskState;
using Task4.DAL.Entities;

namespace Task4.BLL.MappingProfiles
{
    public sealed class TaskProfile : Profile
    {
        public TaskProfile()
        {
            //CreateMap<Task, TaskDTO>()
            //    .ForMember(dest => dest.PerformerName, src => src.MapFrom(s => s.Performer != null ? $"{s.Performer.Lastname} {s.Performer.Firstname}" : string.Empty))
            //    .ForMember(dest => dest.ProjectName, src => src.MapFrom(s => s.Project != null ? s.Project.Name : string.Empty));
            
            CreateMap<Task, TaskDTO>();
            CreateMap<TaskDTO, Task>();
            CreateMap<TaskCreateDTO, Task>();
        }
    }
}
