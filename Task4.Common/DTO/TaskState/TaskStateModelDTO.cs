﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task4.Common.DTO.TaskState
{
    public class TaskStateModelDTO
    {
        public string Value { get; set; }
    }
}
