﻿using System;
using System.Collections.Generic;
using System.Text;
using Task4.Common.DTO.Project;
using Task4.Common.DTO.TaskState;
using Task4.Common.DTO.User;


namespace Task4.Common.DTO.Task
{
    public class TaskDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime FinishedAt { get; set; }
        //public string ProjectName { get; set; }
        public ProjectDTO Project { get; set; }
        //public string PerformerName { get; set; }
        public UserDTO Performer { get; set; }
    }
}
