﻿using System;
using System.Collections.Generic;
using System.Text;
using Task4.Common.DTO.Project;

namespace Task4.Common.DTO.Task
{
    public class TasksCountForUserDTO
    {
        public Dictionary<ProjectDTO, int> Tasks { get; set; }
    }
}
