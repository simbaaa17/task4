﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task4.Common.DTO.Task
{
    public class TaskInformationShortDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
