﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task4.Common.DTO.Task
{
    public class TaskCreateDTO
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public DateTime FinishedAt { get; set; }

        public int ProjectId { get; set; }
        public int PerformerId { get; set; }
    }
}
