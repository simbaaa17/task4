﻿using System;
using System.Collections.Generic;
using System.Text;
using Task4.Common.DTO.Task;
using Task4.Common.DTO.Team;
using Task4.Common.DTO.User;

namespace Task4.Common.DTO.Project
{
    public class ProjectDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime Deadline { get; set; }

        //public string AuthorName { get; set; }
        public UserDTO Author { get; set; }

        //public string TeamName { get; set; }
        public TeamDTO Team { get; set; }


    }
}
