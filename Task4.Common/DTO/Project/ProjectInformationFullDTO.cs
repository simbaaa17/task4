﻿using System;
using System.Collections.Generic;
using System.Text;
using Task4.Common.DTO.Task;

namespace Task4.Common.DTO.Project
{
    public class ProjectInformationFullDTO
    {
        public ProjectDTO Project { get; set; }
        public TaskDTO LongestTask { get; set; }
        public TaskDTO ShortestTask { get; set; }
        public int TeamCount { get; set; }
    }
}
