﻿using System;
using System.Collections.Generic;
using System.Text;
using Task4.Common.DTO.User;

namespace Task4.Common.DTO.Team
{
    public class TeamInformationFullDTO
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public ICollection<UserDTO> Members { get; set; }
    }
}
