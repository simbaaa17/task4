﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task4.Common.DTO.Team
{
    public class TeamCreateDTO
    {
        public string Name { get; set; }
    }
}
