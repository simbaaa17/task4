﻿using System;
using System.Collections.Generic;
using System.Text;
using Task4.Common.DTO.Project;
using Task4.Common.DTO.User;

namespace Task4.Common.DTO.Team
{
    public class TeamDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}
