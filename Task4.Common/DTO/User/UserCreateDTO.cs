﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task4.Common.DTO.User
{
    public class UserCreateDTO
    {
        public string Firstname { get; set; }

        public string Lastname { get; set; }

        public string Email { get; set; }

        public DateTime Birthday { get; set; }

        public DateTime RegisteredAt { get; set; }

        public int? TeamId { get; set; }
    }
}
