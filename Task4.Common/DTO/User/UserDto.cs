﻿using System;
using System.Collections.Generic;
using System.Text;
using Task4.Common.DTO.Task;
using Task4.Common.DTO.Team;

namespace Task4.Common.DTO.User
{
    public class UserDTO
    {
        public int Id { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Email { get; set; }
        public DateTime Birthday { get; set; }
        public DateTime RegisteredAt { get; set; }
        public TeamDTO Team { get; set; }
        public ICollection<TaskDTO> Tasks { get; set; }
    }
}
