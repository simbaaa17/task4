﻿using System;
using System.Collections.Generic;
using System.Text;
using Task4.Common.DTO.Project;
using Task4.Common.DTO.Task;

namespace Task4.Common.DTO.User
{
    public class UserInformationFullDTO
    {
        public UserDTO User { get; set; }

        public ProjectDTO LastProject { get; set; }

        public int TasksCount { get; set; }

        public int TasksCanceledCount { get; set; }

        public TaskDTO LongestTask { get; set; }
    }
}
