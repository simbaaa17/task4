﻿using System;
using System.Collections.Generic;
using System.Text;
using Task4.Common.DTO.Task;

namespace Task4.Common.DTO.User
{
    public class UserWithTasksDTO
    {
        public UserDTO User { get; set; }
        public List<TaskDTO> Tasks { get; set; }
    }
}
